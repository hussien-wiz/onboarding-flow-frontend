const KSA = ['Riyadh', 'Jeddah', 'Dammam', 'Al-Khobar', 'Dhahran', 'Al-Ahsa', 'Qatif', 'Jubail', 'Taif', 'Tabouk', 'Abha', 'Al Baha', 'Jizan', 'Najran', 'Hail', 'Makkah AL-Mukkaramah', 'AL-Madinah Al-Munawarah', 'Al Qaseem', 'Jouf', 'Yanbu'];
const UEA = ['Dubai', 'Abu Dhabi', 'Sharjah', 'Al Ain', 'Fujairah', 'Ajman', 'Ras Al Khaimah', 'Umm Al Quwain'];
const Egypt = ['Cairo', 'Giza', 'Luxor', 'Alexandria', 'Al Fayoum', 'Al Minya', 'Aswan'];

const countries = document.getElementById('countries');
let cities = document.getElementById('cities');

countries.addEventListener('change', (e) => {
    let options = '';
    if (e.target.value === 'UAE') {
        for (let i = 0; i < UEA.length; i++) {
            options += `<option value="${UEA[i]}">${UEA[i]}</option>`;
        }
        cities.innerHTML = options;
    }
    if (e.target.value === 'KSA') {
        for (let i = 0; i < KSA.length; i++) {
            options += `<option value="${KSA[i]}">${KSA[i]}</option>`;
        }
        cities.innerHTML = options;
    }
    if (e.target.value === 'Egypt') {
        for (let i = 0; i < Egypt.length; i++) {
            options += `<option value="${Egypt[i]}">${Egypt[i]}</option>`;
        }
        cities.innerHTML = options;
    }
    if (e.target.value === 'None') {
        cities.innerHTML = '';
    }
});


$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allPrevBtn = $('.prevBtn'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='radio']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest("input").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    allPrevBtn.click(function(){

        var curStep = $(this).closest(".setup-content"),

            curStepBtn = curStep.attr("id"),

            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');

    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    // disable button
    $('#popmake-wrap .button-green').prop('disabled', true);
    $('#popmake-wrap .popupscroll').scroll(function () {
        if ($(this).scrollTop() + $(this).height() > $(this)[0].scrollHeight - 80) {
            //enable button
            $('#popmake-wrap .button-green').prop('disabled', false);
        }
    });
});


// *************** FUNCTIONS ************** //

function showBussiness() {
    $('#business-input').css('display', 'block');
    $('#hidden-indv').val('');
}

function hideBussiness() {
    $('#business-input').css('display', 'none');
    $('#hidden-indv').val('none');
}

function onlyPhoneNumbers(id) {
    const phoneNumber = $(`#${id}`).val();
    if(isNaN(phoneNumber) || phoneNumber.length > 10) {
        let filteredNumber = phoneNumber.substring(0, phoneNumber.length -1);
        $(`#${id}`).val(filteredNumber);
        $(`#${id}`).addClass('invalid-input');
    } else {
        // $(`#${id}`).removeClass('invalid-input');
        return;
    }
}

function productsLoop(e) {
    console.log();
    if ($(e.target).hasClass('active-product')) {
        $(e.target).removeClass('active-product');
        productsContainer.splice(productsContainer.indexOf(e.target.innerText), 1);
        console.log(productsContainer);
    } else {
        $(e.target).addClass('active-product');
        productsContainer.push(e.target.innerText);
        console.log(productsContainer);
    }

    if (productsContainer.length === 0) {
        $('#selection-input').val('');
    } else {
        $('#selection-input').val('test');
    }
}

function skipProductsForNow() {
    $('#selection-input').val('test');
}

// products array
let productsContainer = [];
// this is the variable which holds the string of products where you can use.
let productsString = '';
productsLoop();

function changeArrToStr() {
    productsString = productsContainer.join(',');
    console.log(productsString);
}

function showSite() {
    $('#website-input').css('display', 'block');
    $('#hidden-site').val('');
}

function hideSite() {
    $('#website-input').css('display', 'none');
    $('#hidden-site').val('none');
}


// alert danger
function showDangerAlert() {
    $('#error-alert').fadeIn();
}

// if success response
function validResponse() {
    $('#dontWorry').fadeOut(() => {
        $('#terms').fadeIn();
    });
}

function ifUserGoesBack() {
    $('#terms').fadeOut(() => {
        $('#dontWorry').fadeIn();
    });
}

// your ajax request GOES here!
function ajaxRequest() {

}

// change checkbox value
function changeMyValue() {
    const checkBox = document.getElementById('check-me');
    if (checkBox.checked === false) {
        checkBox.value = 'no';
        console.log(checkBox.value);
    } else {
        if (checkBox.checked === true) {
            checkBox.value = 'yes';
            console.log(checkBox.value);
        }
    }
}


// disable btn until checked
function toggleDisability() {
    const checkBox = document.getElementById('terms-checkbox');
    const submitBtn = document.getElementById('submit-form');
    if (checkBox.checked === false) {
        checkBox.value = 'no';
        submitBtn.disabled = true;
        console.log(checkBox.value);
    } else {
        if (checkBox.checked === true) {
            checkBox.value = 'yes';
            submitBtn.disabled = false;
            console.log(checkBox.value);
        }
    }
}

// terms and condition //


$('.terms-div').scroll(function () {
    console.log('asdasdasd');
});




function scrollToTerms(e) {
    // console.log(e)
    const reachMe = document.querySelector('.reach-me').offsetTop;
    const termsDiv = document.querySelector('.terms-div').scrollTop;

    console.log(termsDiv+200, reachMe);
    if ((termsDiv + 350 ) >= reachMe) {
        $('.terms-footer').css('opacity', '1');
        // alert('asd');
    }
}

// check if the re-password is match the previous one

function checkPass() {
    const pass = document.getElementById('pass');
    const rePass = document.getElementById('rePass');
    // const textDanger = document.getElementById('pass-incorrect');
    if (rePass.value === pass.value) {
        pass.classList.remove('is-danger');
        rePass.classList.remove('is-danger');
        $('#pass-incorrect').css('display', 'none');
    } else {
        pass.classList.add('is-danger');
        rePass.classList.add('is-danger');
        $('#pass-incorrect').css('display', 'block');
    }
}


// toggle pass visibility

function showPwd(id, el) {
    let x = document.getElementById(id);
    if (x.type === "password") {
        x.type = "text";
        el.className = 'fa fa-eye-slash show-icon';
    } else {
        x.type = "password";
        el.className = 'fa fa-eye show-icon';
    }
}
